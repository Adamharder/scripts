
import urllib
import socket


def getInternal():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(("gmail.com",80))
	ip = s.getsockname()[0]
	s.close()
	return ip

def getExternal():
	s = urllib.urlopen("http://whatsmyip.net/").read()
	ip=s.split("<title>")[1]
	ip=ip.split("</title>")[0].strip()
	return ip


print getInternal()
print getExternal()