# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText

def sendGmail(sender, password, recipient, subject, message):

  smtpUrl="smtp.gmail.com:587"

  msg = MIMEText(message, "html")
  msg['Subject'] = subject
  msg['From'] = sender
  msg['To'] = recipient

  smtp = smtplib.SMTP(smtpUrl)
  smtp.starttls()
  smtp.login(sender, password)
  smtp.sendmail(sender, [recipient], msg.as_string())
  smtp.quit()


if __name__=="__main__":
  import json
  cache=json.loads(open("/root/cache.json").read())
  pw=cache["email"]["password"]
  sender=cache["email"]["account"]
  import datetime
  sendGmail(sender, pw, "adam@adamharder.com","test%s" % str(datetime.datetime.now()),"<table border=1><tr><td>test</td><td>number 1</td></tr></table>")
  #sendGmail()